#!/bin/bash

pip3 install tensorflow-gpu --user
pip3 install numpy --user
pip3 install scipy --user
pip3 install opencv-python --user
pip3 install pillow --user
pip3 install h5py --user
pip3 install keras --user
pip3 install keras-retinanet --user
pip3 install https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.2/imageai-2.0.2-py3-none-any.whl  --user
