# CompVisual - Object Detection and Tracking 

### Universidade Presbiteriana Mackenzie (UPM)

### Alunos: 
**RENAN MARCOS DE AMORIM**

**PEDRO SODRE**

---



### Motivação do Projeto

*Detectar objetos de imagens e vídeos com precisão tem sido altamente bem-sucedido na segunda década do século XXI, devido ao aumento dos algoritmos
de aprendizado de máquina e aprendizagem profunda. Algoritmos especializados foram desenvolvidos que podem detectar, localizar e reconhecer objetos em imagens e vídeos, 
alguns dos quais incluem RCNNs, SSD, RetinaNet, **YOLO** , e outros.

O uso desses algoritmos para detectar e reconhecer objetos em vídeos requer um entendimento de matemática aplicada e conhecimento técnico sólido dos algoritmos, 
além de milhares de linhas de código. Este é um processo altamente técnico e demorado, e para aqueles que desejam implementar a detecção de objetos, 
o processo pode ser muito inconveniente*

### Detalhes do Projeto

Neste projeto, utilziaremos a biblioteca em Python chamada **ImageAI** que torna fácil a manipulação para o desenvolvimento de aplicativos ou 
sistemas que possam vir a detectar objetos em vídeos utilizando apenas algumas linhas de código . O ImageIA suporta o algoritimo de detecção
de objetos chamado YOLOv3 , o qual será utilizado durante todo o projeto.

Inicialmente, será necessário a instalação de algumas bibliotes Python . Caso já possua as bibliotecas lsitadas abaixo , apenas cetifique-se que a 
versão do Python instalado na sua máquina corresponda ao Python3.

### *Instalação da libs necessárias via comando:*

Caso esteja utilizando o Jupyter-notebook , siga os comandos abaixo: 

```
!pip install tensorflow-gpu
!pip3 install numpy 
!pip3 install scipy 
!pip3 install opencv-python
!pip3 install pillow 
!pip3 install h5py 
!pip3 install keras
!pip3 install keras-retinanet
!pip3 install https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.2/imageai-2.0.2-py3-none-any.whl
```

Agora que instalamos as ferramentas necessárias, usaremos um modelo de visão computacional YOLOv3 treinado para executar as tarefas de 
detecção e reconhecimento.

Este modelo é treinado para detectar e reconhecer 80 objetos diferentes, nomeados abaixo:

*pessoa, bicicleta, carro, motocicleta, avião, autocarro, trem, caminhão, barco, semáforo, 
stop-sign, parquímetro, banco, ave, gato, cão, cavalo, ovelhas, vaca, elefante, urso, 
girafa mochila, guarda-chuva, bolsa, gravata, mala, frisbee, esquis, snowboard, bola de esportes, 
pipa, taco de beisebol, luva de beisebol, skate, prancha, raquete de tênis, garrafa, copo de vinho, 
copo, garfo, faca, colher, tigela, banana, maçã, sanduíche, laranja, brócolis, cenoura, cachorro-quente, 
pizza, donot, bolo, cadeira, sofá, planta em vaso, cama, mesa de jantar, banheiro, tv, laptop, rato, remoto, 
teclado, telefone celular, microondas, forno, torradeira, pia, geladeira, livro, relógio, vaso, tesoura, 
ursinho de pelúcia, secador de cabelo, escova de dentes.*

---

### Bibliotecas Utilizadas 

`from imageai.Detection import VideoObjectDetection  #Carrega a classe VideoObjectDetection`

`import os`

`import cv2`

---

### Utilizando GDrive como Diretorio de Arquivo(Caso utilize o Google Colab)

`from google.colab import drive `


`drive.mount("/content/gdrive",force_remount=True) `

---


### Definindo um diretório padrão

`ROOT_PATH = "/content/gdrive/My Drive" `

`VIDEO_PATH =  os.path.join(ROOT_PATH, "seu_diretorio")`

`os.chdir(VIDEO_PATH)`

`execution_path = VIDEO_PATH` 

---

### Aplicando o modelo de Treinamento YOLO

`detector = VideoObjectDetection() #Instância a classe VideoObjectDetection`

`detector.setModelTypeAsYOLOv3() #Define o modelo de treinamento a ser utilizado`

`detector.setModelPath( os.path.join(execution_path , "yolo.h5")) #Define o local do  arquivo contento algoritimo de treinamento `

`detector.loadModel()  #Carrega o modelo treinamento para ser aplicado `

---

### Carregando arquivo de Vídeo 

`video_path = detector.detectObjectsFromVideo ( input_file_path = os.path.join (execution_path, "pessoas.mp4"),
                                output_file_path = os.path.join (execution_path, "pessoas_deep")
                                , frames_per_second = 29 , log_progress = True )`

`print(video_path) #Exibe a saida do log `

---


### Explicação de parâmetros utilizados :

Na chamada **video_path = detector.detectObjectsFromVideo(...)** temos alguns parâmetros que podem ser utilizados : 

**input_file_path** : Isso se refere ao caminho do arquivo de vídeo copiado para pasta 

**output_file_path**: Refere-se ao caminho no qual o arquivo de vídeo gerado pelo algoritimo será salvo , juntamento com a sua extensão final (.avi)

**frames_per_second**: Refere-se ao número de frames(quadros) de imagem que queremos que o vídeo detectado tenha dentro de um segundo (fps)

**log_progress**: Isso é usado apra indicar que a instância de detecção deve relatar o progresso frame a frame  na linha de comando.

**detectObjectsFromVideo**: A função retornará o caminho do arquivo do vídeo detectado. Este caminho de arquivo será impresso na ultima linha do código, assim que a tarefa de detecção estiver concluída.


---


**Artigos e Repositórios  Utilizados**

**Detecting objects in videos and camera feeds using Keras, OpenCV, and ImageAI**

Disponpivel em : https://heartbeat.fritz.ai/detecting-objects-in-videos-and-camera-feeds-using-keras-opencv-and-imageai-c869fe1ebcdb

  https://github.com/OlafenwaMoses/ImageAI/

**R-CNN, Fast R-CNN, Faster R-CNN, YOLO — Object Detection Algorithms**

Disponpivel em : https://towardsdatascience.com/r-cnn-fast-r-cnn-faster-r-cnn-yolo-object-detection-algorithms-36d53571365e

**Gentle guide on how YOLO Object Localization works with Keras**

Disponpivel em : https://heartbeat.fritz.ai/gentle-guide-on-how-yolo-object-localization-works-with-keras-part-2-65fe59ac12d


---

**Repositório da Biblioteca Principal do Projeto**

**ImageAI: https://github.com/OlafenwaMoses/ImageAI/**Desenvolvedor : Moses Olafenwa 

--- 

**Somshubra Majumdar, DenseNet Implementation of the paper, Densely Connected Convolutional Networks in Keras**
https://github.com/titu1994/DenseNet/ 


**Broad Institute of MIT and Harvard, Keras package for deep residual networks** 
https://github.com/broadinstitute/keras-resnet 


**Fizyr, Keras implementation of RetinaNet object detection**
https://github.com/fizyr/keras-retinanet 


**Francois Chollet, Keras code and weights files for popular deeplearning models**
https://github.com/fchollet/deep-learning-models 

**Tsung-Yi. et al, Focal Loss for Dense Object Detection**
https://arxiv.org/abs/1708.02002 


**O Russakovsky et al, ImageNet Large Scale Visual Recognition Challenge**
https://arxiv.org/abs/1409.0575 


**TY Lin et al, Microsoft COCO: Common Objects in Context**
https://arxiv.org/abs/1405.0312 


**Moses & John Olafenwa, A collection of images of identifiable professionals.**
https://github.com/OlafenwaMoses/IdenProf 


**Joseph Redmon and Ali Farhadi, YOLOv3: An Incremental Improvement.**
https://arxiv.org/abs/1804.02767 

  




