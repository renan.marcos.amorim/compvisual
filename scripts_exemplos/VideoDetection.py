# -*- coding: utf-8 -*-
"""
Autores: RENAN MARCOS DE AMORIM | PEDRO SODRÉ
"""

"""
Antes de executarmos nosso código Python, 
aqui está uma explicação detalhada do código abaixo:
"""
from imageai.Detection import VideoObjectDetection
import os

execution_path = os.getcwd()

detector = VideoObjectDetection() #Instância da classe VideoObjectDetection
detector.setModelTypeAsYOLOv3()  #Definimos o tipo de modelo para YOLOV3(Utilizado no treinamento)
detector.setModelPath( os.path.join(execution_path , "yolo.h5"))
detector.loadModel()  #- -> Realiza o treinamento basenado no YOLO  

video_path = detector.detectObjectsFromVideo(input_file_path=os.path.join( execution_path, "VID_20190511_181223.mp4"),
                                output_file_path=os.path.join(execution_path, "VID_20190511_181223_detected_1")
                                , frames_per_second=29, log_progress=True)
print(video_path)